package com.cyut.database._.schema.administrativedivisions;

import com.cyut.database._.schema.administrativedivisions.generated.GeneratedAdministrativeDivisionsManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * com.cyut.database._.schema.administrativedivisions.AdministrativeDivisions}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public final class AdministrativeDivisionsManagerImpl 
extends GeneratedAdministrativeDivisionsManagerImpl 
implements AdministrativeDivisionsManager {}