package com.cyut.database._.schema.members;

import com.cyut.database._.schema.members.generated.GeneratedMembersManager;

/**
 * The main interface for the manager of every {@link
 * com.cyut.database._.schema.members.Members} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public interface MembersManager extends GeneratedMembersManager {}