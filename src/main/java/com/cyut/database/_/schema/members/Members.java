package com.cyut.database._.schema.members;

import com.cyut.database._.schema.members.generated.GeneratedMembers;

/**
 * The main interface for entities of the {@code members}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public interface Members extends GeneratedMembers {}