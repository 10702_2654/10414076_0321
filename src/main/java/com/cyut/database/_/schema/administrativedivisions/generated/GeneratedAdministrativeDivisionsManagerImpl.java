package com.cyut.database._.schema.administrativedivisions.generated;

import com.cyut.database._.schema.administrativedivisions.AdministrativeDivisions;
import com.cyut.database._.schema.administrativedivisions.AdministrativeDivisionsManager;
import com.speedment.common.annotation.GeneratedCode;
import com.speedment.runtime.config.identifier.TableIdentifier;
import com.speedment.runtime.core.manager.AbstractManager;
import com.speedment.runtime.field.Field;

import java.util.stream.Stream;

/**
 * The generated base implementation for the manager of every {@link
 * com.cyut.database._.schema.administrativedivisions.AdministrativeDivisions}
 * entity.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public abstract class GeneratedAdministrativeDivisionsManagerImpl 
extends AbstractManager<AdministrativeDivisions> 
implements GeneratedAdministrativeDivisionsManager {
    
    private final TableIdentifier<AdministrativeDivisions> tableIdentifier;
    
    protected GeneratedAdministrativeDivisionsManagerImpl() {
        this.tableIdentifier = TableIdentifier.of("", "schema", "administrativeDivisions");
    }
    
    @Override
    public TableIdentifier<AdministrativeDivisions> getTableIdentifier() {
        return tableIdentifier;
    }
    
    @Override
    public Stream<Field<AdministrativeDivisions>> fields() {
        return AdministrativeDivisionsManager.FIELDS.stream();
    }
    
    @Override
    public Stream<Field<AdministrativeDivisions>> primaryKeyFields() {
        return Stream.of(
            AdministrativeDivisions.ROWID
        );
    }
}