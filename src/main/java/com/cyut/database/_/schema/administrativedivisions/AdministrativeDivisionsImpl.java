package com.cyut.database._.schema.administrativedivisions;

import com.cyut.database._.schema.administrativedivisions.generated.GeneratedAdministrativeDivisionsImpl;

/**
 * The default implementation of the {@link
 * com.cyut.database._.schema.administrativedivisions.AdministrativeDivisions}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public final class AdministrativeDivisionsImpl 
extends GeneratedAdministrativeDivisionsImpl 
implements AdministrativeDivisions {}