package com.cyut.database._.schema.administrativedivisions;

import com.cyut.database._.schema.administrativedivisions.generated.GeneratedAdministrativeDivisions;

/**
 * The main interface for entities of the {@code administrative divisions}-table
 * in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public interface AdministrativeDivisions extends GeneratedAdministrativeDivisions {}