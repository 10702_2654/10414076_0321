package com.cyut.database._.schema.members;

import com.cyut.database._.schema.members.generated.GeneratedMembersImpl;

/**
 * The default implementation of the {@link
 * com.cyut.database._.schema.members.Members}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public final class MembersImpl 
extends GeneratedMembersImpl 
implements Members {}