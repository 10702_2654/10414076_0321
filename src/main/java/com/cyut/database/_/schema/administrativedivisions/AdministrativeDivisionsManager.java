package com.cyut.database._.schema.administrativedivisions;

import com.cyut.database._.schema.administrativedivisions.generated.GeneratedAdministrativeDivisionsManager;

/**
 * The main interface for the manager of every {@link
 * com.cyut.database._.schema.administrativedivisions.AdministrativeDivisions}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public interface AdministrativeDivisionsManager extends GeneratedAdministrativeDivisionsManager {}