package com.cyut.database._.schema.administrativedivisions;

import com.cyut.database._.schema.administrativedivisions.generated.GeneratedAdministrativeDivisionsSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.cyut.database._.schema.administrativedivisions.AdministrativeDivisions}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public class AdministrativeDivisionsSqlAdapter extends GeneratedAdministrativeDivisionsSqlAdapter {}