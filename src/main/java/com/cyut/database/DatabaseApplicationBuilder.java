package com.cyut.database;

import com.cyut.database.generated.GeneratedDatabaseApplicationBuilder;

/**
 * The default {@link com.speedment.runtime.core.ApplicationBuilder}
 * implementation class for the {@link com.speedment.runtime.config.Project}
 * named database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public final class DatabaseApplicationBuilder extends GeneratedDatabaseApplicationBuilder {}