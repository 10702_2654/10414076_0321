package com.cyut.database;

import com.cyut.database.generated.GeneratedDatabaseApplication;

/**
 * An {@link com.speedment.runtime.core.ApplicationBuilder} interface for the
 * {@link com.speedment.runtime.config.Project} named database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author CYUT
 */
public interface DatabaseApplication extends GeneratedDatabaseApplication {}