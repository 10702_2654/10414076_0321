package sample;

import com.cyut.database.DatabaseApplication;
import com.cyut.database.DatabaseApplicationBuilder;
import com.cyut.database._.schema.administrativedivisions.AdministrativeDivisions;
import com.cyut.database._.schema.administrativedivisions.AdministrativeDivisionsImpl;
import com.cyut.database._.schema.administrativedivisions.AdministrativeDivisionsManager;
import com.speedment.runtime.core.ApplicationBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public GridPane Main;
    public RadioButton sexMale , sexFemale;
    public DatePicker birthPicker;
    public ToggleGroup btnSEX;
    public Button btnUpload , btnConnectDB;
    public ImageView avatar;
    public ComboBox boxBlood , boxAdministrativeDivisions;

    public void selectBirthday(ActionEvent actionEvent) {
        System.out.println(birthPicker.getValue());
    }

    public void selectSex(ActionEvent actionEvent) {
        if (sexMale.isSelected()){
            System.out.println(sexMale.getText());
        }else{
            System.out.println(sexFemale.getText());
        }//
    }

    public void selectUpload(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File selectedFile = fileChooser.showOpenDialog(Main.getScene().getWindow());
        avatar.setImage(new Image(selectedFile.toURI().toString()));

        System.out.println(selectedFile.getAbsolutePath());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /*ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("A" , "B" , "O" , "AB");
        boxBlood.setItems(items);

        ObservableList<String> itemsdivisions = FXCollections.observableArrayList();
        String fileURL = "D:\\IdeaProjects\\10414076_0321\\src\\main\\java\\administrative divisions.txt";
        String line;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileURL)));
            while ((line=bufferedReader.readLine())!=null){
                itemsdivisions.add(line);
            }

            bufferedReader.close();
            boxAdministrativeDivisions.setItems(itemsdivisions);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        DatabaseApplication databaseApplication = new DatabaseApplicationBuilder()
                .withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.STREAM)
                .withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.UPDATE)
                .build();
        AdministrativeDivisionsManager administrativeDivisionsManager = databaseApplication.getOrThrow(AdministrativeDivisionsManager.class);
        administrativeDivisionsManager.stream().forEach(System.out::println);
        AdministrativeDivisions administrativeDivisions = new AdministrativeDivisionsImpl().setCityname("New Taipei City").setRowid(administrativeDivisionsManager.stream().count() + 1);
        administrativeDivisionsManager.persist(administrativeDivisions);

        List<String> citys = new ArrayList<>();
        administrativeDivisionsManager.stream().forEach(e->{
            if (e.getCityname().isPresent()){
                citys.add(e.getCityname().get());
            }
        });
        ObservableList<String> observableList = FXCollections.observableList(citys);
        boxAdministrativeDivisions.setItems(observableList);
    }

    public void selectConnect(ActionEvent actionEvent) {

    }
}
